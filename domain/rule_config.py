# -*- coding: utf-8 -*-
# @Time    : 2024/1/2  22:10
# @Author  : zhanghao
# @FileName: RuleConfig.py
# @Software: PyCharm
"""
    Description:
        
"""
from domain.rule import Rule
from config.logger import Logger


class RuleConfig:
    logger = Logger().logger
    version = ''
    name = ''
    rules: list[Rule] = []

    def __int__(self, version, name):
        self.name = name
        self.version = version

    def __init__(self, version, name, rules):
        self.version = version
        self.name = name

        self.rules = []
        for singleRule in rules:
            rule = Rule(singleRule['type'], singleRule['keyword'], singleRule['separator'], singleRule['rename'])
            self.rules.append(rule)

        self.logger.info('规则初始化成功')

    def versionCompare(self, version):
        if version and float(version) > float(self.version):
            return True
        else:
            return False
