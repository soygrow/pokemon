# -*- coding: utf-8 -*-
# @Time    : 2024/1/4  16:12
# @Author  : zhanghao
# @FileName: SignalUnit.py
# @Software: PyCharm
"""
    Description: 用作通知UI线程更新界面时的参数
"""
from domain.table_info import TableInfo
from helper.status import Status
from helper.event_type import EventType


class SignalUnit(object):
    event_type = EventType.NONE
    task_total = 0
    task_no = 0
    task_failed = 0
    task_status = Status.PROCESSING
    data_before = ''
    data_after = ''
    manual_op = False

    def __init__(self):
        pass

    def __str__(self):
        return f"SignalUnit(event_type={self.event_type}, task_total={self.task_total}, " \
               f"task_no={self.task_no}, data_before={self.data_before}, " \
               f"data_status={self.task_status}, data_after={self.data_after})"

    @staticmethod
    def build(event_type: EventType, task_status, data: TableInfo):
        signal_unit = SignalUnit()
        signal_unit.event_type = event_type
        signal_unit.task_status = task_status

        if data:
            signal_unit.task_no = data.idx
            signal_unit.data_before = data.before_name
            signal_unit.data_after = data.after_name
        if task_status == Status.FAILED:
            signal_unit.manual_op = True

        return signal_unit

