# author: zhanghao
# time: 2023-12-27
# info: 列表信息类
from helper.status import Status


class TableInfo:
    idx = 0
    before_name = ''
    status = Status.WAIT_PROCESS
    after_name = ''
    operate = False

    def __init__(self, idx, before_name, status: Status, after_name=None, operate=None):
        self.idx = idx
        self.before_name = before_name
        self.status = status
        if after_name is not None:
            self.after_name = after_name
        if operate is not None:
            self.operate = operate


