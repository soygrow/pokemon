# -*- coding: utf-8 -*-
# @Time    : 2024/1/2  22:09
# @Author  : zhanghao
# @FileName: Rename.py
# @Software: PyCharm
"""
    Description:
        
"""


class Rename:
    name = ''
    match_type = ''
    info = ''

    def __init__(self, name, match_type, info):
        self.name = name
        self.match_type = match_type
        self.info = info

    def get_one(self):
        return self.name