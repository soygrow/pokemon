# -*- coding: utf-8 -*-
# @Time    : 2024/1/2  22:10
# @Author  : zhanghao
# @FileName: Rule.py
# @Software: PyCharm
"""
    Description:
        
"""
from domain.rename import Rename


class Rule:
    type = ''
    keyword = ''
    separator = ''
    rename: list[Rename] = []

    def __init__(self, type, keyword, separator, rename):
        self.type = type
        self.keyword = keyword
        self.separator = separator

        self.rename = []
        for idx in range(len(rename)):
            rename_obj = Rename(rename[idx]['name'], rename[idx]['matchType'], rename[idx]['info'])
            self.rename.append(rename_obj)

    def get_one(self):
        return self.rename[0].get_one()