# author: zhanghao
# time: 2023-12-26
# info: 这是主函数文件

# This is a pokemon Python script.
from PyQt6.QtWidgets import QApplication, QDialog
from PyQt6 import QtCore

from multiprocessing import Pool, Pipe

from core.pokemon import Pokemon
from core.worker import Worker
from config.config import Config

import mul_process_package
import multiprocessing
import sys

# 初始化配置
config = Config()
config.initConfig()

# main
if __name__ == '__main__':
    multiprocessing.freeze_support()

    # 信号-用于主子进程之间通信
    conn_main, conn_sub = Pipe()
    worker = Worker(conn_sub, config)
    worker.start()

    # 启动应用
    app = QApplication(sys.argv)
    mainWindow = QDialog()
    pokemon = Pokemon(conn_main, config)
    pokemon.setupUi(mainWindow)
    mainWindow.setSizeGripEnabled(True)
    mainWindow.setWindowFlag(QtCore.Qt.WindowType.WindowMinMaxButtonsHint)
    mainWindow.show()

    sys.exit(app.exec())


