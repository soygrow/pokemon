import os
import re
import time
from unittest import TestCase

from config.config import Config
from helper.status import Status
from core.prcess import Process
from domain.table_info import TableInfo


class TestConfig(TestCase):

    def test_list(self):
        process_result: list[TableInfo]
        process_result = []
        data1 = TableInfo(2, 'b2', Status.WAIT_PROCESS)
        data2 = TableInfo(1, 'b1', Status.WAIT_PROCESS)
        data3 = TableInfo(4, 'b4', Status.WAIT_PROCESS)
        data4 = TableInfo(3, 'b3', Status.WAIT_PROCESS)
        process_result.append(data1)
        process_result.append(data2)
        process_result.append(data3)
        process_result.append(data4)

        process_result = sorted(process_result, key=lambda x: x.idx)
        for idx in process_result:
            print(str(idx.idx) + ' ' + idx.before_name)

    def test_regex_case_no(self):
        reg = '（[1-2]{1}[0-9]{3}）[京津冀晋蒙辽吉黑沪苏浙皖闽赣鲁豫鄂湘粤桂琼川贵云渝藏陕甘青宁新]{1}[0-9]+[\u4e00-\u9fa5]+[0-9]+号(之一|之二|之三|之四|之五|之六|之七|之八|之九|之十|){1}'
        pattern = re.compile(reg)
        matchObj = re.search(pattern, 'ass手动阀（2023）冀0303民初2279号之九fasd')

    def test_defendant_name(self):
        reg = '(被告|被申请人|被执行人)[：:]{0,1}([\u4e00-\u9fa5]+)[,，。]{1}'
        pattern = re.compile(reg)
        search_result = re.findall(pattern, 'ass手被执行人：张三明。9号之九。被申请人:张三明阿是，fa被张三明提升到，sd')
        result = ''
        if search_result and len(search_result) > 0:
            for defendant in search_result:
                if len(result) == 0:
                    result = defendant[1]
                else:
                    result = result + '、' + defendant[1]
        matchObj = re.findall(pattern, 'ass手升被申请人到，sd')

    def test_rename(self):
        os.rename('/Users/hancheng/Desktop/123_案件_调解书',
                  '/Users/hancheng/Desktop/《Python程序设计基础及实践（慕课版）》.zip')

    def test_load_config(self):
        s = '执行书'
        ps = '你是开始看执行书上课的话'
        config = Process()

    def test_process(self):
        table_data: list[TableInfo] = []
        table_info = TableInfo('/Users/hancheng/Desktop/project/test/刘伟撤诉裁定.pdf', '正在处理')
        table_data.append(table_info)
        process = Process()
        process.process(table_data)

    def test_process_img(self):
        table_data: list[TableInfo] = []
        table_info = TableInfo('/Users/hancheng/Desktop/project/test/232.png', '正在处理')
        table_data.append(table_info)
        process = Process()
        process.process(table_data)
    def test_load_config1(self):
        s = '执行书'
        ps = '你是开始看执行书上课的话'
        config = Config()
        config.initConfig()

        while 1:
            time.sleep(3)

    def test_other(self):
        current_path = os.getcwd()

