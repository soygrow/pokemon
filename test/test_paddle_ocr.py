# -*- coding: utf-8 -*-
# @Time    : 2023/12/29  00:20
# @Author  : zhanghao
# @FileName: test_paddle_ocr.py
# @Software: PyCharm
"""
    Description:
        
"""
import os
from unittest import TestCase

from fitz import fitz
from paddle.dataset.image import cv2
from paddleocr import PaddleOCR, PPStructure
from paddleocr.ppstructure.predict_system import save_structure_res
from paddleocr.ppstructure.recovery.recovery_to_doc import sorted_layout_boxes, convert_info_docx

from core.paddle_ocr import PaddleOcr


class TestPaddleOcr(TestCase):
    paddle_ocr = PaddleOcr()

    def test_pdf2img(self):
        content = self.paddle_ocr.processPdf('/Users/hancheng/Desktop/project/批量回传材料/驳回裁定/港城支行张焕廷驳回起诉裁定.pdf')


    def test_img2text(self):
        self.paddle_ocr.processImg('/Users/hancheng/Desktop/ocrDir')
