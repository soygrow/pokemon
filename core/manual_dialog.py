# -*- coding: utf-8 -*-
# @Time    : 2024/1/5  15:08
# @Author  : zhanghao
# @FileName: manual_window.py
# @Software: PyCharm
"""
    Description: 手动处理弹框
"""
import os.path

from PyQt6.QtWidgets import QTableWidgetItem, QMainWindow, QInputDialog, QHBoxLayout, QLabel
from helper.file_helper import FileHelper
from config.logger import Logger


class ManualDialog(QInputDialog):

    before_name = ''

    logger = Logger().get()

    def __init__(self, before_name):
        super().__init__()
        self.before_name = before_name
        self.createDialog()

    def createDialog(self):
        self.setOkButtonText('提交')
        self.setCancelButtonText("取消")
        file_name, ok = QInputDialog.getText(self, '手动操作', '请输入文件名: ')
        if ok and self.before_name:
            self.logger.info(file_name)
            # 重命名
            if os.path.exists(self.before_name):
                file_path = FileHelper.fetchDirName(self.before_name)
                file_type = FileHelper.getFileType(self.before_name)
                file_name = file_path + '/' + file_name + file_type

                self.logger.info(file_name)
                self.logger.info(self.before_name)
                os.rename(self.before_name, file_name)
                self.logger.info('重命名成功.' + self.before_name + ' ' +file_name)
