# -*- coding: utf-8 -*-
# @Time    : 2024/1/2  21:32
# @Author  : zhanghao
# @FileName: ocr_result.py
# @Software: PyCharm
"""
    Description: 定义ocr结果
"""
from config.logger import Logger


class OcrResult:
    logger = Logger().get()
    text = ""
    ocr_result = []

    def __init__(self, ocr_result: list):
        self.logger.info(ocr_result)
        if ocr_result:
            self.ocr_result = ocr_result
            self.text = ''.join(ocr_result)
