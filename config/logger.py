# -*- coding: utf-8 -*-
# @Time    : 2024/1/6  20:34
# @Author  : zhanghao
# @FileName: logger.py
# @Software: PyCharm
"""
    Description: 日志模块
"""
import os

from loguru import logger


class Logger:

    logger: logger

    def __init__(self):
        BASE_DIR = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
        logger.add(os.path.join(BASE_DIR, "../logs/{time:YYYY-MM-DD}.log"),
                   format="{time: YYYY-MM-DD at HH:mm:ss} | {level} | {message}",
                   rotation="00:00", retention=3, encoding='utf-8')

        self.logger = logger

    def get(self):
        return self.logger

