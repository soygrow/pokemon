# author: zhanghao
# time: 2023-12-26
# info: 工具类
import os.path


class FileHelper:

    @staticmethod
    def fetchDirFile(base_dir):
        files = [os.path.join(base_dir, file) for file in os.listdir(base_dir)]

        deal_files = []
        for file in files:
            if os.path.isfile(file):
                if not FileHelper.isHiddenFile(file):
                    deal_files.append(file)
            elif os.path.isdir(file):
                deap_dir_files = FileHelper.fetchDirFile(file)
                for deap_file in deap_dir_files:
                    deal_files.append(deap_file)

        return deal_files

    @staticmethod
    def isHiddenFile(base_file):
        file_name = os.path.basename(base_file)
        if file_name.startswith('.'):
            return True
        else:
            return False

    @staticmethod
    def getFileType(file_dir):
        file_type = ''
        if file_dir:
            file_type = os.path.splitext(file_dir)[1]
        return file_type

    @staticmethod
    def fetchFileType(file_dir):
        if file_dir:
            file_type = os.path.splitext(file_dir)[1]
            if file_type.lower() == '.pdf':
                return 1
            elif file_type.lower() in ['.png', '.jpg', '.jpeg']:
                return 2
            else:
                return 0
        else:
            return 0

    @staticmethod
    def fetchFileName(file_path):
        file_name = os.path.basename(file_path)
        return file_name

    @staticmethod
    def fetchDirName(file_path):
        dir_name = os.path.dirname(file_path)
        return dir_name
