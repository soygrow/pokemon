# -*- coding: utf-8 -*-
# @Time    : 2024/1/4  17:54
# @Author  : zhanghao
# @FileName: event_type.py
# @Software: PyCharm
"""
    Description: 定义UI更新事件类型
"""
from enum import Enum


class EventType(Enum):
    NONE = 1
    START_PROCESS = 2
    END_PROCESS = 3
