# author: zhanghao
# time: 2023-12-26
# info: 状态类
from enum import Enum


class Status(Enum):
    WAIT_PROCESS = 1
    PROCESSING = 2
    SUCCESS = 3
    FAILED = 4

    @staticmethod
    def description(status):
        desc = '等待处理'
        if status is None:
            return desc

        if status == Status.SUCCESS:
            desc = '处理成功'
        elif status == Status.PROCESSING:
            desc = '正在处理'
        elif status == Status.FAILED:
            desc = '处理失败'
        else:
            desc = '等待处理'

        return desc
