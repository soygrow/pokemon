# OCR重命名工具

打包命令：pyinstaller -D -w  main.py --collect-all paddleocr --add-data "./3library/mklml.dll;."

## 问题
1. 使用pyinstaller打包后，运行时会遇到各种包不存在，需要手动查询包，并将包拷贝到_internal目录下：
- imageio
- imgaug
- lazy_loader
- lmdb
- pyclipper
- skimage

以下是需要拷贝到_internal/scipy目录中
- scipy的io
- scipy的cluster
删除目录中“scipy的”

文件“scipy的_lib中的_disjoint_set.py”需要拷贝到_internal/scipy/_lib中，并删除文件名中的“scipy的_lib中的”

2. python-假的单线程
因为GIL是Python解释器中的全局锁(Global Interpreter Lock)，它是一种机制，确保同一时刻只有一个线程在执行Python代码。在多线程程序中，因为GIL的存在，多个线程并不能真正并行地执行，而是通过竞争GIL的方式来实现对CPU的占用。这也是为什么Python中的多线程程序并不比单线程程序更快的原因之一。

本工程是含有GUI的，采用多线程，后台任务线程是CPU密集型的，需要长占用CPU，所以导致运行时，虽然是多线程，但是界面还是会卡，所以这里改成多进程后，就不会有这个问题了

3. pyinstaller打包多进程问题
pyinstaller打包多进程python项目成exe，在windows中运行时发现会不断地创建进程，具体原因我没有研究，我用processexploer查看发现程序运行时，命令行带有--multiprocessing-fork，后查找资料，在项目中增加mul_process_package.py文件：
```
import os
import sys
import multiprocessing

# Module multiprocessing is organized differently in Python 3.4+
try:
    # Python 3.4+
    if sys.platform.startswith('win'):
        import multiprocessing.popen_spawn_win32 as forking
    else:
        import multiprocessing.popen_fork as forking
except ImportError:
    import multiprocessing.forking as forking

if sys.platform.startswith('win'):
    # First define a modified version of Popen.
    class _Popen(forking.Popen):
        def __init__(self, *args, **kw):
            if hasattr(sys, 'frozen'):
                # We have to set original _MEIPASS2 value from sys._MEIPASS
                # to get --onefile mode working.
                os.putenv('_MEIPASS2', sys._MEIPASS)
            try:
                super(_Popen, self).__init__(*args, **kw)
            finally:
                if hasattr(sys, 'frozen'):
                    # On some platforms (e.g. AIX) 'os.unsetenv()' is not
                    # available. In those cases we cannot delete the variable
                    # but only set it to the empty string. The bootloader
                    # can handle this case.
                    if hasattr(os, 'unsetenv'):
                        os.unsetenv('_MEIPASS2')
                    else:
                        os.putenv('_MEIPASS2', '')


    # Second override 'Popen' class with our modified version.
    forking.Popen = _Popen

```

main.py中增加：
```
multiprocessing.freeze_support()
```
解决了这个问题